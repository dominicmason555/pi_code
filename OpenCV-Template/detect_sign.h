#ifndef DETECT_SIGN_H_INCLUDED
#define DETECT_SIGN_H_INCLUDED

#include "main.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "pi2c.h"
using namespace cv;
int detect_sign(Mat image, switcher * state);
void setup_sign(Pi2c * board);
#endif
