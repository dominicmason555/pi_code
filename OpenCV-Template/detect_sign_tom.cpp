// Include files for required libraries
#include <stdio.h>

#include "opencv_aee.hpp"
#include "main.hpp"     // You can use this file for declaring defined values and functions
#include "detect_sign.h"

#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;


int detect_sign (Mat image, switcher * state)
{
    Mat image_HSV;
    Mat image_BW;

    int rownumber = 0;

    //image = imread("Test_Offset.jpg"); // Read the image file
    //resize(image, image, Size(), 0.25, 0.25);

    cvtColor(image, image_HSV, COLOR_BGR2HSV); // Convert the image to HSV
    inRange(image_HSV, Scalar(135, 50, 50), Scalar(175, 255, 255), image_BW); // Filter the image for pink

    imshow("before", image_BW);
    waitKey(1);

    vector<vector<Point> > contours; // Variable for list of contours
    vector<Vec4i> hierarchy; // Variable for image topology data
    findContours(image_BW, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, Point(0, 0)); // Calculate the contours and store them
    int n = contours.size();

    int a = 0;
    int b = 0;
    int i = 0;

    for (i = 0; i<n; i++)
    {
        int area = contourArea(contours[i]);

        if (area>a)
        {
            a = area;
            b = i;
        }
    }

    int m = 0;

    Mat drawing = Mat::zeros( image_BW.size(), CV_8UC3);
    for( size_t m = 0; m<contours.size(); m++)
    {
        Scalar color = Scalar(255, 255, 255);
        drawContours(drawing, contours, (int)m, color, 2, 8, hierarchy, 0, Point() );
    }
    imshow("contours", drawing);
    waitKey(1);

    std::vector< std::vector<cv::Point> > approxedcontours(contours.size());
    for(int j = 0; j < contours.size(); j++)
    {
        cv::approxPolyDP(contours[j],approxedcontours[j], 10, true);
    }

    image_BW = transformPerspective(approxedcontours[b], image_BW, 320, 240);
    if(image_BW.cols > 0 && image_BW.rows > 0)
    {
        imshow("transform", image_BW);
        waitKey(1);

        Mat images[10];
        float match [10];
        float biggestMatch = 0;
        int imageNumber;

        images [0] = readImage("BlueShortcut.PNG");
        images [1] = readImage("GreenShortcut.PNG");
        images [2] = readImage("YellowShortcut.PNG");
        images [3] = readImage("RedShortcut.PNG");
        images [4] = readImage("FollowBlack.PNG");
        images [5] = readImage("DistanceMeasurement.PNG");
        images [6] = readImage("ShapeCounter.PNG");
        images [7] = readImage("StopLight.PNG");
        images [8] = readImage("InclineMeasurement.PNG");
        images [9] = readImage("Football.PNG");

        for(i = 0; i < 10; i++)
        {
            cvtColor(images[i], images[i], COLOR_BGR2HSV); // Convert the image to HSV
            inRange(images[i], Scalar(140, 125, 255), Scalar(200, 255, 255), images[i]);
            match [i] = compareImages(image_BW,images[i]);
            cout << i << " percentage " << match[i] << endl;
            if(match[i]>biggestMatch)
            {
                biggestMatch = match[i];
                imageNumber = i;
            }
        }


        char colour = 'Z';

        if (imageNumber == 0)
        {
            colour = 'A';
        }
        else if (imageNumber == 1)
        {
            colour = 'B';
        }
        else if (imageNumber == 2)
        {
            colour = 'C';
        }

        else if (imageNumber == 3)
        {
            colour = 'D';
        }

        else if (imageNumber == 4)
        {
            colour = 'E';
        }

        else if (imageNumber == 5)
        {
            colour = 'F';
        }

        else if (imageNumber == 6)
        {
            colour = 'G';
        }

        else if (imageNumber == 7)
        {
            colour = 'H';
        }

        else if (imageNumber == 8)
        {
            colour = 'I';
        }
        else
        {
            colour = 'J';
        }

        char speech[50];

        switch(colour)
        {
        case 'A' :
            cout << "Blue" << endl;
            snprintf(speech, 50, "Blue line");
            system(speech);
            break;
        case 'B' :
            cout << "Green" << endl;
            snprintf(speech, 50, "Green line");
            system(speech);
            break;
        case 'C' :
            cout << "Yellow" << endl;
            snprintf(speech, 50, "Yellow line");
            system(speech);
            break;
        case 'D' :
            cout << "Red" << endl;
            snprintf(speech, 50, "Red line");
            system(speech);
            break;
        case 'E' :
            cout << "Black" << endl;
            snprintf(speech, 50, "Black line");
            system(speech);
            break;
        case 'F' :
            cout << "Distance" << endl;
            snprintf(speech, 50, "Blue line");
            system(speech);
            break;
        case 'G' :
            cout << "Shape" << endl;
            snprintf(speech, 50, "Shape counting detected");
            system(speech);
            break;
        case 'H' :
            cout << "Stop" << endl;
            snprintf(speech, 50, "stop light");
            system(speech);
            break;
        case 'I' :
            cout << "Incline" << endl;
            snprintf(speech, 50, "measure incline");
            system(speech);
            break;
        case 'J' :
            cout << "Football" << endl;
            snprintf(speech, 50, "yay football");
            system(speech);
            break;
        default :
            cout << "None" << endl;
            break;
        }
    }


}

