#ifndef MAIN_HPP_INCLUDED
#define MAIN_HPP_INCLUDED

//#pragma once
//
//#include <stdio.h>
//#include <iostream>
#include <opencv2/opencv.hpp>
//#include <wiringPi.h>
//
////#include "main.hpp"
//#include "detect_sign.h"
//#include "detect_line.h"
//#include "opencv_aee.hpp"
#include "pi2c.h"
//#include "MiniPID.h"

//using namespace cv;

// Function declarations
//void setup(void);                   //Function to configure the GPIO and devices for operation
//int main(int argc, char** argv);
enum state_t {GREEN, YELLOW, RED, BLUE, BALL, LIGHT, RAMP, COUNT, DISTANCE, BLACK, SIGN};

class switcher
{
private:
	state_t state;
public:
	switcher(state_t st) : state(st){changeState(st);};
	void changeState(state_t st);
	void run(cv::Mat frame);
};

#endif // MAIN_HPP_INCLUDED
