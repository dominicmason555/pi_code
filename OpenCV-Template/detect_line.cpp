// Include files for required libraries
#include <stdio.h>

#include "opencv_aee.hpp"
#include "main.hpp"     // You can use this file for declaring defined values and functions
#include "detect_sign.h"
#include "MiniPID.h"
#include "pi2c.h"
#include <wiringPi.h>
#include <softPwm.h>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <ctime>

#define SWITCH_MODES 1

using namespace std;
using namespace cv;

/* This union simplifies converting an int to the two bytes
   that make it up, since the i2c library won't let you send
   it whole*/
union CarSpeed
{
    int speed; // The whole int
    char byte[2]; // The two bytes that make it up
};

Mat masks[2]; // Masks for selecting parts of the line
MiniPID * pid; // PID controller
int backup; // Last known line position
const int baseSpeed = 25; // Speed in a straight line
// used to be 15
const int limitSpeed = 30; // Maximum speed of a wheel either way
// used to be 20
state_t colour = BLACK; // Colour to follow, BLACK means black
clock_t started;

void setup_line(state_t c, Pi2c * board)
{
    // Runs once when switchin to line following mode
    colour = c; // Change to whichever colour main.cpp says
    cout << "Switching to line " << colour << "following" << endl;
    masks[0] = imread("bLine0.png", 0); // mask for the bottom
    masks[1] = imread("bLine1.png", 0); // mask for the top
    pid = new MiniPID(0.35,0,0.1); // Kp, Ki and Kd
    //Shaky but working: 1.1, 0.08, 4.8
    pid->setOutputLimits(-limitSpeed,limitSpeed); // speed limit
    pinMode(12, OUTPUT);
    softPwmCreate(12, 10, 200);
    char packet[3] = {'L', 0, 255};
    board->i2cWrite(packet, 3);
    packet[0] = 'R';
    board->i2cWrite(packet, 3);
    delay(2000);
    softPwmStop(12);
    started = clock();
    resizeCamera(320, 240);
}

void detectLine(Mat image, switcher * state, Pi2c * board)
{
    Mat grey, out, hsv, detect;

    cvtColor(image, grey, COLOR_BGR2GRAY); // greyscale for black
    cvtColor(image, hsv, COLOR_BGR2HSV); // hsv for coloured lines


    // ====================== Detecting Markers ============================


    inRange(hsv, Scalar(140, 50, 50), Scalar(170, 255, 255), detect);
    if (SWITCH_MODES)
    {
        clock_t now = clock();
        if ((double(now - started)/ CLOCKS_PER_SEC) >= 5)
        {
            int numPink = countNonZero(detect);
            cout << "Pink Number : " << numPink << endl;
            if (numPink > 1500)
            {
                state->changeState(SIGN);
            }
        }
    }


    // ====================== Isolating the line ===========================


    Mat colours[5]; // Each colour isolated to its own image
    // Red1, Red2, Green, Blue, Yellow (red needs two because it wraps around)
    inRange(hsv, Scalar(0, 50, 50), Scalar(15, 255, 255), colours[0]);
    inRange(hsv, Scalar(170, 50, 50), Scalar(180, 255, 255), colours[1]);
    inRange(hsv, Scalar(35, 50, 20), Scalar(75, 255, 255), colours[2]);
    //inRange(hsv, Scalar(35, 70, 70), Scalar(75, 255, 255), colours[2]);
    inRange(hsv, Scalar(90, 50, 50), Scalar(140, 255, 255), colours[3]);
    inRange(hsv, Scalar(15, 70, 70), Scalar(33, 255, 255), colours[4]);

    Mat lineShape; // Image of the correct line, other colours removed
    // Make lineShape white on the line and black everywhere else
    switch(colour)
    {
    case BLACK:
        for (int i = 0; i < 5; i++)
            bitwise_or(grey, colours[i], grey); // make colours white
        // so dark colours don't look like the black line
        threshold(grey, lineShape, 70, 255, 1); // detect the black
        break;
    case RED:
        bitwise_or(colours[0], colours[1], lineShape); // Both ends of red
        break;
    case GREEN:
        lineShape = colours[2]; // Green image
        break;
    case YELLOW:
        lineShape = colours[4]; // Yellow image
        break;
    case BLUE:
        lineShape = colours[3]; // Blue image
        break;
    default: // if the colour is wrong somehow
        cout << "\n\n\nINVALID COLOUR FOR LINE FOLLOWING\n\n\n" << endl;
    }


    // ======================= Finding the centre ============================


    /* Two centres are found, one at the bottom and one at the top, this is
     * so the car can predict when the line is going to go out of frame,
     * since the top of the line is going to go out of frame before the
     * bottom does. When the top goes out of frame, it makes a backup of the
     * bottom so it can use that when it cant find the line so it turns the
     * right way to find the line.*/

    Point centres[2]; // There's the bottom centre and another at the top
    for (int i = 0; i < 2; i++)
    {
        Mat sides; // Storage for section to consider
        bitwise_and(lineShape, masks[i], sides); // Isolate top or bottom
        Moments m = moments(sides, false); // Calculate moments of section
        centres[i] = Point(m.m10/m.m00, m.m01/m.m00); // Find centre of line
        circle(lineShape, centres[i], 5, Scalar(0,0,0)); // Highlight centre
    }
    // countNonZero would be a good way of validating a good line position

    // Back up valid bottom position if top invalid
    if (centres[1].x <= 0 || centres[1].x >= 320)
        if (centres[0].x > 0 || centres[0].x < 320)
            backup = centres[0].x;
    // Restore backup if bottom invalid
    if (centres[0].x <= 0 || centres[0].x >= 320)
        centres[0].x = backup;


    // ======================= Outputting the speed ============================


    double output = pid->getOutput(centres[0].x, 160); // Get PID output,
    // 160 is the middle of the vehicle because its half of 320

    CarSpeed leftWheel; // The union for splitting ints into bytes
    CarSpeed rightWheel; // one for each wheel
    leftWheel.speed = baseSpeed + output; // set the speed as an integer
    rightWheel.speed = baseSpeed - output;
    // speed one wheel up and slow the other one down so the vehicle turns

    char lPacket[3] = {'L', leftWheel.byte[1], leftWheel.byte[0]};
    char rPacket[3] = {'R', rightWheel.byte[1], rightWheel.byte[0]};
    /* This is the packet the car board takes, L or R for left or right wheels
     * and the two bytes of the speed, taken from the union*/

    //cout << "Centre:" << centres[0].x << "Left Wheel:" << leftWheel.speed << "Right Wheel:" << rightWheel.speed << endl;

    board->i2cWrite(lPacket, 3); // Send the packets to the car board
    board->i2cWrite(rPacket, 3); // The 3 is because they are 3 bytes long

    imshow("Cleaned", grey); // Show the colour removal
    imshow("Lines", lineShape); // Show the centre detection
    imshow("Pink", detect);

}









// ==============================================================================================================
// ============================Everything past here is old code that isnt used===================================
// ==============================================================================================================














//pid.setOutputRampRate(10);
/* Stop the car
    if (centres[0].x == 0)
    {
        leftWheel.speed = 0;
        rightWheel.speed = 0;
    }
	*/
/*
    //for (int i = 1; i <= 2; i+=2)
    //    medianBlur(grey, grey, i);
    //int siz = 2;
    //Mat element = getStructuringElement(MORPH_ELLIPSE, Size(2*siz + 1, 2 * siz + 1), Point(siz, siz));
    //erode(grey, grey, element);
	*/
/*
int top = (int) (0.05*grey.rows);
int bottom = top;
int left = (int) (0.05*grey.cols);
int right = left;
Scalar value(255, 255, 255);
copyMakeBorder(grey, grey, top, bottom, left, right, BORDER_CONSTANT, value);
*/

/*
byte1 = speed ^ 0x00FF;
byte2 = (speed ^ 0xFF00) >> 2;
*/
/*

vector<vector<Point> > contours;
vector<Vec4i> hierarchy;
Canny(grey, out, 50, 100, 3);
findContours(out, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, Point(0, 0));

Mat drawing = Mat::zeros( out.size(), CV_8UC3 );
for( size_t i = 0; i< contours.size(); i++ )
{
    Scalar color = Scalar(255,255,255);
    drawContours( drawing, contours, (int)i, color, 5, 8, hierarchy, 0, Point() );
}
*/

/*
Mat lowLine = imread("line1.png");
threshold(lowLine, lowLine, 180, 255, 0);
Mat isect;
bitwise_and(lowLine, drawing, isect);
imshow("Intersect", isect);
*/
/*
threshold(drawing, drawing, 180, 255, 0);
uchar *point = drawing.ptr<uchar>(200);
int xTotal = 0, xCount = 0;
for (int i = 0; i < drawing.cols; i++)
{
    //point[i] = 100;
    cout << i << " "  << (int)point[i] << " ";
    if (point[i] != 0)
    {
        xCount++;
        xTotal += i;
    }

}
int linePos = -1;
if (xCount != 0)
    linePos = xTotal / xCount;
cout << "Average:" << linePos << endl;
*/
