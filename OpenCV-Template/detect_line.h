#ifndef DETECT_LINE_H_INCLUDED
#define DETECT_LINE_H_INCLUDED
#include "pi2c.h"
#include "MiniPID.h"
#include "main.hpp"

void detectLine(Mat image, switcher * state, Pi2c * board);
void setup_line(state_t c, Pi2c * board);

#endif // DETECT_LINE_H_INCLUDED
