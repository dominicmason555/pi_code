// Include files for required libraries
#include <stdio.h>

#include "opencv_aee.hpp"
#include "main.hpp"     // You can use this file for declaring defined values and functions
#include "detect_sign.h"
#include "pi2c.h"

#include <iostream>
#include <wiringPi.h>
#include <softPwm.h>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

int lastSign;
int signCount;

Mat image_BW;
Mat image_G;
Mat image_Y;
Mat image_R;
Mat image_B;
Mat image_K;
Mat image_Dist;
Mat image_Ball;
Mat image_Ramp;
Mat image_Count;
Mat image_Light;

void setup_sign (Pi2c * board)
{
    //wiringPiSetup();
    cout << "Switiching to sign detection" << endl;
    char packet[3] = {'L', 0, 0};
    board->i2cWrite(packet, 3);
    packet[0] = 'R';
    board->i2cWrite(packet, 3);
    pinMode(12, OUTPUT);
    softPwmCreate(12, 18, 200);

    lastSign = -1;
    signCount = 0;
    resizeCamera(640, 480);

    // ================== LOADING SIGNS =========================
    image_K = imread("FollowBlack.PNG");
    cvtColor(image_K, image_K, COLOR_BGR2HSV); // Convert the image to HSV
    inRange(image_K, Scalar(140, 125, 255), Scalar(200, 255, 255), image_K); // Filter the image for pink
    printf("laoded");
    image_G = imread("GreenShortCut.PNG");
    cvtColor(image_G, image_G, COLOR_BGR2HSV); // Convert the image to HSV
    inRange(image_G, Scalar(140, 125, 255), Scalar(200, 255, 255), image_G); // Filter the image for pink

    image_Y = imread("YellowShortCut.PNG");
    cvtColor(image_Y, image_Y, COLOR_BGR2HSV); // Convert the image to HSV
    inRange(image_Y, Scalar(140, 125, 255), Scalar(200, 255, 255), image_Y); // Filter the image for pink

    image_R = imread("RedShortCut.PNG");
    cvtColor(image_R, image_R, COLOR_BGR2HSV); // Convert the image to HSV
    inRange(image_R, Scalar(140, 125, 255), Scalar(200, 255, 255), image_R); // Filter the image for pink

    image_B = imread("BlueShortCut.PNG");
    cvtColor(image_B, image_B, COLOR_BGR2HSV); // Convert the image to HSV
    inRange(image_B, Scalar(140, 125, 255), Scalar(200, 255, 255), image_B); // Filter the image for pink

    image_Ball = imread("Football.PNG");
    cvtColor(image_Ball, image_Ball, COLOR_BGR2HSV); // Convert the image to HSV
    inRange(image_Ball, Scalar(140, 125, 255), Scalar(200, 255, 255), image_Ball);

    image_Ramp = imread("InclineMeasurement.PNG");
    cvtColor(image_Ramp, image_Ramp, COLOR_BGR2HSV); // Convert the image to HSV
    inRange(image_Ramp, Scalar(140, 125, 255), Scalar(200, 255, 255), image_Ramp);

    image_Light = imread("StopLight.PNG");
    cvtColor(image_Light, image_Light, COLOR_BGR2HSV); // Convert the image to HSV
    inRange(image_Light, Scalar(140, 125, 255), Scalar(200, 255, 255), image_Light);

    image_Count = imread("ShapeCounter.PNG");
    cvtColor(image_Count, image_Count, COLOR_BGR2HSV); // Convert the image to HSV
    inRange(image_Count, Scalar(140, 125, 255), Scalar(200, 255, 255), image_Count);

    image_Dist = imread("DistanceMeasurement.PNG");
    cvtColor(image_Dist, image_Dist, COLOR_BGR2HSV); // Convert the image to HSV
    inRange(image_Dist, Scalar(140, 125, 255), Scalar(200, 255, 255), image_Dist);

    delay(1000);
    softPwmCreate(12, 150, 200);
    delay(1000);
    softPwmStop(12);
}

int find_sign (Mat image)
{
    cout << "hello?" << endl;
    Mat image_HSV;

    int rownumber = 0;

    //image = imread("Test_Offset.jpg"); // Read the image file
    //resize(image, image, Size(), 0.25, 0.25);

    cvtColor(image, image_HSV, COLOR_BGR2HSV); // Convert the image to HSV
    inRange(image_HSV, Scalar(135, 50, 50), Scalar(175, 255, 255), image_BW); // Filter the image for pink
    printf("loading");


    imshow("before", image_BW);
    waitKey(1);
    cout << "line 99" << endl;
    vector<vector<Point> > contours; // Variable for list of contours
    vector<Vec4i> hierarchy; // Variable for image topology data
    findContours(image_BW, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, Point(0, 0)); // Calculate the contours and store them
    int n = contours.size();

    cout << "Number of contours:" << n << endl;

    int a = 0;
    int b = 0;
    int i = 0;

    for (i = 0; i<n; i++)
    {
        int area = contourArea(contours[i]);

        if (area>a)
        {
            a = area;
            b = i;
        }
    }

    //cout << "biggest contour:" << b << endl;

    int m = 0;
    cout << "line 125" << endl;
    Mat drawing = Mat::zeros( image_BW.size(), CV_8UC3);
    for( size_t m = 0; m<contours.size(); m++)
    {
        //cout << "Cn: " << (int)m << endl;
        Scalar color = Scalar(255, 255, 255);
        drawContours(drawing, contours, (int)m, color, 2, 8, hierarchy, 0, Point() );
    }
    imshow("contours", drawing);
    waitKey(1);
    cout << "line 135" << endl;
    cout << contours.size() << endl;
    if (contours.size() < 1)
        return -1;
    std::vector< std::vector<cv::Point> > approxedcontours(contours.size());
    for(int j = 0; j < contours.size(); j++)
    {
        cv::approxPolyDP(contours[j],approxedcontours[j], 10, true);
    }
    //cout << approxedcontours[b] << endl;
    if (approxedcontours[b].size() <= 0)
        return -1;
    cout << "about to transform" << endl;

    image_BW = transformPerspective(approxedcontours[b], image_BW, 320, 240);
    if(image_BW.cols > 0 && image_BW.rows > 0)
    {
        imshow("transform", image_BW);

        waitKey(1);
        cout << "about to die" << endl;
        float match_K = compareImages(image_BW, image_K);
        float match_G = compareImages(image_BW, image_G);
        float match_Y = compareImages(image_BW, image_Y);
        float match_R = compareImages(image_BW, image_R);
        float match_B = compareImages(image_BW, image_B);
        float match_Ball = compareImages(image_BW, image_Ball);
        float match_Ramp = compareImages(image_BW, image_Ramp);
        float match_Light = compareImages(image_BW, image_Light);
        float match_Count = compareImages(image_BW, image_Count);
        float match_Dist = compareImages(image_BW, image_Dist);

        cout << "percantage black:" << match_K << endl;
        cout << "percentage green:" << match_G << endl;
        cout << "percentage yellow:" << match_Y << endl;
        cout << "percentage red:" << match_R << endl;
        cout << "percentage blue:" << match_B << endl;
        cout << "percentage ball:" << match_Ball << endl;
        cout << "percentage light:" << match_Light << endl;
        cout << "percentage ramp:" << match_Ramp << endl;
        cout << "percentage count:" << match_Count << endl;
        cout << "percentage distance:" << match_Dist << endl;

        int matches[10];
        matches[0] = match_K;
        matches[1] = match_G;
        matches[2] = match_Y;
        matches[3] = match_R;
        matches[4] = match_B;
        matches[5] = match_Ball;
        matches[6] = match_Ramp;
        matches[7] = match_Light;
        matches[8] = match_Count;
        matches[9] = match_Dist;


        int highestMatch = 0, match = 0;
        for (int i = 0; i < 9; i++)
        {
            if (matches[i] > highestMatch)
            {
                highestMatch = matches[i];
                match = i;
            }
        }

        /*
        int matches[9];
        matches[0] = match_K;
        matches[1] = match_G;
        matches[2] = match_Y;
        matches[3] = match_R;
        matches[4] = match_B;
        matches[5] = match_Ball;
        matches[6] = match_Ramp;
        matches[7] = match_Light;
        matches[8] = match_Dist;

        */

        switch(match)
        {
        case 1 :
            cout << "Green" << endl;
            return GREEN;
            break;
        case 2 :
            cout << "Yellow" << endl;
            return YELLOW;
            break;
        case 3 :
            cout << "Red" << endl;
            return RED;
            break;
        case 4 :
            cout << "Blue" << endl;
            return BLUE;
            break;
        case 5 :
            cout << "Ball" << endl;
            return BALL;
            break;
        case 7 :
            cout << "Light" << endl;
            return LIGHT;
            break;
        case 6 :
            cout << "Ramp" << endl;
            return RAMP;
            break;
        case 8 :
            cout << "Count" << endl;
            return COUNT;
            break;
        case 9 :
            cout << "Distance" << endl;
            return DISTANCE;
            break;
        case 0 :
            cout << "None" << endl;
            return BLACK;
            break;
        }

    }
    return -1;
}

int detect_sign (Mat image, switcher * state)
{
    cout << "check" << endl;
    int sign = find_sign(image);
    if (sign < -1 || sign > 9)
    {
        cout << "sign machine broke" << endl;
        exit(1);
    }
    if (lastSign == -1)
        lastSign = sign;
    else if (sign == lastSign)
        signCount++;
    if (signCount > 5 && lastSign != -1)
    {
        cout << "\n\nGOOD SIGN FOUND\n\n" << endl;
        state->changeState((state_t)lastSign);
    }
    if (signCount > 5 && lastSign == -1)
    {
        cout << "\n\nNO SIGN FOUND\n\n" << endl;
        state->changeState(BLACK);
    }
}


/*
char colour = 'Z';

if ((match_G>match_Y) && (match_G>match_R) && (match_G>match_B) && (match_G>match_Dist) && (match_G>match_Ball) && (match_G>match_Ramp) && (match_G>match_Light) && (match_G>match_Count))
{
    colour = 'A';
}

if ((match_Y>match_G) && (match_Y>match_R) && (match_Y>match_B) && (match_Y>match_Dist) && (match_Y>match_Ball) && (match_Y>match_Ramp) && (match_Y>match_Light) && (match_Y>match_Count))
{
    colour = 'B';
}

if ((match_R>match_Y) && (match_R>match_G) && (match_R>match_B) && (match_R>match_Dist) && (match_R>match_Ball) && (match_R>match_Ramp) && (match_R>match_Light) && (match_R>match_Count))
{
    colour = 'C';
}

if ((match_B>match_Y) && (match_B>match_R) && (match_B>match_G) && (match_B>match_Dist) && (match_B>match_Ball) && (match_B>match_Ramp) && (match_B>match_Light) && (match_B>match_Count))
{
    colour = 'D';
}

if ((match_Ball>match_Y) && (match_Ball>match_R) && (match_Ball>match_G) && (match_Ball>match_B) && (match_Ball>match_Light) && (match_Ball>match_Dist) && (match_Ball>match_Ramp) && (match_Ball>match_Count))
{
    colour = 'E';
}

if ((match_Light>match_Y) && (match_Light>match_R) && (match_Light>match_G) && (match_Light>match_B) && (match_Light>match_Dist) && (match_Light>match_Ball) && (match_Light>match_Ramp) && (match_Light>match_Count))
{
    colour = 'F';
}

if ((match_Ramp>match_Y) && (match_Ramp>match_R) && (match_Ramp>match_G) && (match_Ramp>match_B) && (match_Ramp>match_Light) && (match_Ramp>match_Ball) && (match_Ramp>match_Dist) && (match_Ramp>match_Count))
{
    colour = 'G';
}

if ((match_Count>match_Y) && (match_Count>match_R) && (match_Count>match_G) && (match_Count>match_B) && (match_Count>match_Light) && (match_Count>match_Ball) && (match_Count>match_Ramp) && (match_Count>match_Dist))
{
    colour = 'H';
}

if ((match_Dist>match_Y) && (match_Dist>match_R) && (match_Dist>match_G) && (match_Dist>match_B) && (match_Dist>match_Light) && (match_Dist>match_Ball) && (match_Dist>match_Ramp) && (match_Dist>match_Count))
{
    colour = 'I';
}

if ((match_K>match_Y) && (match_K>match_R) && (match_K>match_B) && (match_K>match_Dist) && (match_K>match_Ball) && (match_K>match_Ramp) && (match_K>match_Light) && (match_G>match_Count))
{
    colour = 'J';
}
*/
