// Include files for required libraries
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include <wiringPi.h>
#include <unistd.h>
#include "rs232.h"
#include <regex>

#include "main.hpp"     // You can use this file for declaring defined values and functions
#include "detect_sign.h"
#include "detect_line.h"
#include "opencv_aee.hpp"
#include "pi2c.h"
#include "MiniPID.h"

//enum sign_t {GREEN, YELLOW, RED, BLUE, BALL, LIGHT, RAMP, COUNT, DISTANCE, BLACK};

using namespace std;


Pi2c board(0x22);
Pi2c sensors(0x07);

int btHandle;

void switcher::changeState(state_t st)
{
    // Cancel movement etc here
    switch(st)
    {
    case BLACK:
    case GREEN:
    case YELLOW:
    case RED:
    case BLUE:
        setup_line(st, &board);
        break;
    case SIGN:
        setup_sign(&board);
        break;
    default:
        cout << "invalid switch" << st << endl;
        st = BLACK;
        setup_line(BLACK, &board);
        break;
    }
    state = st;
};

void switcher::run(Mat frame)
{

    switch(state)
    {
    case BLACK:
    case GREEN:
    case YELLOW:
    case RED:
    case BLUE:
        detectLine(frame, this, &board);
        break;
    case SIGN:
        detect_sign(frame, this);
        break;
    default:
        cout << "invalid run" << state << endl;
        this->changeState(BLACK);
        break;
    }
}

void setup(void)
{
    wiringPiSetupGpio();    // Initialise the wiringPi library

    setupCamera(320, 240);  // Enable the camera for OpenCV
}

int main( int argc, char** argv )
{
    setup();    // Call a setup function to prepare IO and devices

    int i, n, cport_nr=26, bdrate=9600;
    unsigned char buf[4096];
    char mode[] = {'8', 'N', '1', 0};
    bool bt;
    if(bt = (RS232_OpenComport(cport_nr, bdrate, mode)))
        cout << "Bluetooth machine Broke" << endl;
    else
        cout << "Bluetooth machine Fixed" << endl;


    switcher currentState(BLACK);

    while(1)    // Main loop to perform image processing
    {
        Mat frame;

        if (!bt)
        {
            n = RS232_PollComport(cport_nr, buf, 4095);

            if(n > 0)
            {
                buf[n] = 0;   /* always put a "null" at the end of a string! */

                for(i=0; i < n; i++)
                {
                    if(buf[i] < 32)  /* replace unreadable control-codes by dots */
                    {
                        buf[i] = '.';
                    }
                }

                printf("received %i bytes: %s\n", n, (char *)buf);
            }

            cout << buf[i] << endl;

            if (buf[i] == 'A')
            {
                while(frame.empty())
                    frame = captureFrame(); // Capture a frame from the camera and store in a new matrix variable
                imshow("Boi", frame); //Display the image in the window
                currentState.run(frame);
            } else
            {

            }

        }
        else
        {
            while(frame.empty())
                frame = captureFrame(); // Capture a frame from the camera and store in a new matrix variable
            imshow("Boi", frame); //Display the image in the window
            currentState.run(frame);
        }

        int key = waitKey(10);   // Wait 1ms for a keypress (required to update windows)

        key = (key==255) ? -1 : key;    // Check if the ESC key has been pressed
        if (key == 27)
            break;
    }

    closeCV();  // Disable the camera and close any windows

    return 0;
}



