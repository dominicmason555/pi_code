#include <stdio.h>

#include "opencv_aee.hpp"
#include "main.hpp"     // You can use this file for declaring defined values and functions

#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

void setup(void)
{
    /// Setup camera won't work if you don't have a compatible webcam
    //setupCamera(320, 240);  // Enable the camera for OpenCV
}

int main()
{
    Mat image; // Create matrices to store the images
    Mat image_HSV;
    Mat image_BW;

    image = imread("Test.jpg"); // Read the image file
    resize(image, image, Size(), 0.25, 0.25);

    cvtColor(image, image_HSV, COLOR_BGR2HSV); // Convert the image to HSV
    inRange(image_HSV, Scalar(135, 50, 50), Scalar(175, 255, 255), image_BW); // Filter the image for pink

    vector<vector<Point> > contours; // Variable for list of contours
    vector<Vec4i> hierarchy; // Variable for image topology data
    findContours(image_BW, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, Point(0, 0)); // Calculate the contours and store them
    int n = contours.size();

    cout << "Number of contours:" << n << endl;

    int m = 0;

    Mat drawing = Mat::zeros( image_BW.size(), CV_8UC3);
    for( size_t m = 0; m<contours.size(); m++)
    {
        cout << "Cn: " << (int)m << endl;
        Scalar color = Scalar(255, 255, 255);
        drawContours(drawing, contours, (int)m, color, 2, 8, hierarchy, 0, Point() );
    }
    imshow("contours", drawing);
    waitKey();

    int circle = 0;
    int triangle = 0;
    int quadrilateral = 0;
    int pentagon = 0;

    std::vector< std::vector<cv::Point> > approxedcontours(contours.size());
    for(int j = 0; j < contours.size(); j++)
    {
        cv::approxPolyDP(contours[j],approxedcontours[j], 6, true);

        if (approxedcontours[j].size() > 6)
        {
            circle = circle + 1;
        }
        else if (approxedcontours[j].size() == 3)
        {
            triangle = triangle + 1;
        }
        else if (approxedcontours[j].size() == 4)
        {
            quadrilateral = quadrilateral + 1;
        }
        else if (approxedcontours[j].size() == 5)
        {
            pentagon = pentagon + 1;
        }
    }

    cout << "circles: " << circle << endl;
    cout << "triangles: " << triangle << endl;
    cout << "quadrilateral: " << quadrilateral << endl;
    cout << "pentagons: " << pentagon << endl;
}




