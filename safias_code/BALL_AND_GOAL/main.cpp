// Include files for required libraries
#include <stdio.h>

#include "opencv_aee.hpp"

#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

union CarSpeed
{
    int speed; // The whole int
    char byte[2]; // The two bytes that make it up
};

void setup_football(void)
{
    /// Setup camera won't work if you don't have a compatible webcam
    //setupCamera(320, 240);  // Enable the camera for OpenCV
    pinMode(12, OUTPUT);
    softPwmCreate(12, 100, 200);
    delay(2000);
    softPwmStop(12);

    int numGREEN = 0;

    CarSpeed leftWheel;
    CarSpeed rightWheel;
    leftWheel.speed = -20;
    rightWheel.speed = 20;

    char lPacket[3] = {'L', leftWheel.byte[1], leftWheel.byte[0]};
    char rPacket[3] = {'R', rightWheel.byte[1], rightWheel.byte[0]};
    //cout << "Centre:" << centres[0].x << "Left Wheel:" << leftWheel.speed << "Right Wheel:" << rightWheel.speed << endl;
    board->i2cWrite(lPacket, 3);
    board->i2cWrite(rPacket, 3);

    while (numGREEN < 15000)
    {
        image = imread("GREEN.jpg"); // Read the captured image

        cvtColor(image, image_HSV, COLOR_BGR2HSV); // Convert the red image to HSV

        inRange(image_HSV, Scalar(45, 178, 178), Scalar(68, 255, 255), image_GREEN);

        numGREEN = countNonZero(image_GREEN);

    }

    leftWheel.speed = 0;
    rightWheel.speed = 0;

    char lPacket[3] = {'L', leftWheel.byte[1], leftWheel.byte[0]};
    char rPacket[3] = {'R', rightWheel.byte[1], rightWheel.byte[0]};
    //cout << "Centre:" << centres[0].x << "Left Wheel:" << leftWheel.speed << "Right Wheel:" << rightWheel.speed << endl;
    board->i2cWrite(lPacket, 3);
    board->i2cWrite(rPacket, 3);
}

int football()
{
    Mat image; // Create matrices to store the images
    Mat image_HSV;
    Mat image_GREEN;

    int rownumber = 0;

    image = imread("GREEN.jpg"); // Read the captured image

    cvtColor(image, image_HSV, COLOR_BGR2HSV); // Convert the red image to HSV

    inRange(image_HSV, Scalar(45, 178, 178), Scalar(68, 255, 255), image_GREEN);

    Point centre = 0;

    //cout << "boi" << endl;
    Moments m = moments(image_GREEN, false);
    centre = Point(m.m10/m.m00, m.m01/m.m00);
    circle(drawing, centre, 5, Scalar(0,0,0));
    //imshow("sides", sides);

    //pid.setOutputRampRate(10);
    double output = pid->getOutput(centre.x, 160); //179 with border

    CarSpeed leftWheel;
    CarSpeed rightWheel;
    leftWheel.speed = output;
    rightWheel.speed = -output;

    char lPacket[3] = {'L', leftWheel.byte[1], leftWheel.byte[0]};
    char rPacket[3] = {'R', rightWheel.byte[1], rightWheel.byte[0]};
    //cout << "Centre:" << centres[0].x << "Left Wheel:" << leftWheel.speed << "Right Wheel:" << rightWheel.speed << endl;
    board->i2cWrite(lPacket, 3);
    board->i2cWrite(rPacket, 3);

}
