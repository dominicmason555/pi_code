// Include files for required libraries
#include <stdio.h>

#include "opencv_aee.hpp"
//#include "main.hpp"     // You can use this file for declaring defined values and functions

#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;


void setup(void)
{
    /// Setup camera won't work if you don't have a compatible webcam
    //setupCamera(320, 240);  // Enable the camera for OpenCV
}

int main()
{
    Mat image; // Create matrices to store the images
    Mat image_HSV;
    Mat image_RED;
    Mat image_GREEN;

    int rownumber = 0;

    image = imread("GREEN.jpg"); // Read the captured image

    cvtColor(image, image, COLOR_BGR2HSV); // Convert the red image to HSV

    inRange(image, Scalar(0, 191, 204), Scalar(14, 255, 255), image_RED);
    uchar* r = image_RED.ptr<uchar>(rownumber);
    for(int x = 0; x < image_RED.cols; x++)
    {
      r[x];
      uchar pixel = r[x]; // read the data into pixel
      r[x] = 255; // make the pixel white
    }
    int numRED = 0; // Create an integer to store the result
    numRED = countNonZero(image_RED); // Count the number of non-zero pixels

    inRange(image, Scalar(45, 178, 178), Scalar(68, 255, 255), image_GREEN);
    uchar* g = image_GREEN.ptr<uchar>(rownumber);
    for(int x = 0; x < image_GREEN.cols; x++)
    {
      g[x];
      uchar pixel = g[x]; // read the data into pixel
      g[x] = 255; // make the pixel white
    }
    int numGREEN = 0; // Create a integer to store the result
    numGREEN = countNonZero(image_GREEN); // Count the number of non-zero pixels

    if (numGREEN>numRED)
    {
        cout << "GREEN" << endl;
    }
    if (numRED>numGREEN)
    {
        cout << "RED" << endl;
    }

}
