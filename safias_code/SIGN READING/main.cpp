// Include files for required libraries
#include <stdio.h>

#include "opencv_aee.hpp"
#include "main.hpp"     // You can use this file for declaring defined values and functions

#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;


void setup(void)
{
    /// Setup camera won't work if you don't have a compatible webcam
    //setupCamera(320, 240);  // Enable the camera for OpenCV
}

int main ()
{
    Mat image; // Create matrices to store the images
    Mat image_HSV;
    Mat image_BW;
    Mat image_G;
    Mat image_Y;
    Mat image_R;
    Mat image_B;
    int rownumber = 0;

    image = imread("GreenShortCut.png"); // Read the image file

    cvtColor(image, image_HSV, COLOR_BGR2HSV); // Convert the image to HSV
    inRange(image_HSV, Scalar(140, 125, 255), Scalar(200, 255, 255), image_BW); // Filter the image for pink

    image_G = imread("GreenShortCut.png");
    cvtColor(image_G, image_G, COLOR_BGR2HSV); // Convert the image to HSV
    inRange(image_G, Scalar(140, 125, 255), Scalar(200, 255, 255), image_G); // Filter the image for pink

    image_Y = imread("YellowShortCut.png");
    cvtColor(image_Y, image_Y, COLOR_BGR2HSV); // Convert the image to HSV
    inRange(image_Y, Scalar(140, 125, 255), Scalar(200, 255, 255), image_Y); // Filter the image for pink

    image_R = imread("RedShortCut.png");
    cvtColor(image_R, image_R, COLOR_BGR2HSV); // Convert the image to HSV
    inRange(image_R, Scalar(140, 125, 255), Scalar(200, 255, 255), image_R); // Filter the image for pink

    image_B = imread("BlueShortCut.png");
    cvtColor(image_B, image_B, COLOR_BGR2HSV); // Convert the image to HSV
    inRange(image_B, Scalar(140, 125, 255), Scalar(200, 255, 255), image_B); // Filter the image for pink

    uchar* p = image_BW.ptr<uchar>(rownumber);
    for(int x = 0; x < image_BW.cols; x++)
    {
      p[x];
      uchar pixel = p[x]; // read the data into pixel
      p[x] = 255; // make the pixel white
    }

     vector<vector<Point> > contours; // Variable for list of contours
    vector<Vec4i> hierarchy; // Variable for image topology data
    findContours(image_BW, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, Point(0, 0)); // Calculate the contours and store them
    int n = contours.size();
    int a = 0;
    int b = 0;
    int i = 0;

    for (i = 0; i<n; i++)
    {
       int area = contourArea(contours[i]);

       if (area>a)
       {
           a = area;
           b = i;
       }
    }


    image_BW = transformPerspective(contours[b], image_BW, 320, 240);

    //cout << contours[b] << endl;

    //namedWindow("Image"); // Create a window called image
    //imshow("Image", image_TRANSFORMED); // Display the image matrix in the window


    float match_G = compareImages(image_BW, image_G);
    float match_Y = compareImages(image_BW, image_Y);
    float match_R = compareImages(image_BW, image_R);
    float match_B = compareImages(image_BW, image_B);

    char colour = 'A';

    if ((match_G>match_Y) && (match_G>match_R) && (match_G>match_B))
    {
        colour = 'G';
    }

    if ((match_Y>match_G) && (match_Y>match_R) && (match_Y>match_B))
    {
        colour = 'Y';
    }

    if ((match_R>match_Y) && (match_R>match_G) && (match_R>match_B))
    {
        colour = 'R';
    }

    if ((match_B>match_Y) && (match_B>match_R) && (match_B>match_G))
    {
        colour = 'B';
    }

    switch(colour) {
      case 'G' :
         cout << "GREEN" << endl;
         break;
      case 'Y' :
         cout << "YELLOW" << endl;
         break;
      case 'R' :
         cout << "RED" << endl;
         break;
      case 'B' :
         cout << "BLUE" << endl;
         break;
      default :
         cout << "ERROR" << endl;
    }


}
