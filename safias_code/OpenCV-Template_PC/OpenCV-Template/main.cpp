// Include files for required libraries
#include <stdio.h>

#include "opencv_aee.hpp"
#include "main.hpp"     // You can use this file for declaring defined values and functions

#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;


void setup(void)
{
    /// Setup camera won't work if you don't have a compatible webcam
    //setupCamera(320, 240);  // Enable the camera for OpenCV
}

int main ()
{
    Mat image; // Create matrices to store the images
    Mat image_HSV;
    Mat image_BW;
    Mat image_G;
    Mat image_Y;
    Mat image_R;
    Mat image_B;
    Mat image_Dist;
    Mat image_Ball;
    Mat image_Ramp;
    Mat image_Count;
    Mat image_Light;
    int rownumber = 0;

    image = imread("Test_Offset.jpg"); // Read the image file
    resize(image, image, Size(), 0.25, 0.25);

    cvtColor(image, image_HSV, COLOR_BGR2HSV); // Convert the image to HSV
    inRange(image_HSV, Scalar(135, 50, 50), Scalar(175, 255, 255), image_BW); // Filter the image for pink

    image_G = imread("GreenShortCut.png");
    cvtColor(image_G, image_G, COLOR_BGR2HSV); // Convert the image to HSV
    inRange(image_G, Scalar(140, 125, 255), Scalar(200, 255, 255), image_G); // Filter the image for pink

    image_Y = imread("YellowShortCut.png");
    cvtColor(image_Y, image_Y, COLOR_BGR2HSV); // Convert the image to HSV
    inRange(image_Y, Scalar(140, 125, 255), Scalar(200, 255, 255), image_Y); // Filter the image for pink

    image_R = imread("RedShortCut.png");
    cvtColor(image_R, image_R, COLOR_BGR2HSV); // Convert the image to HSV
    inRange(image_R, Scalar(140, 125, 255), Scalar(200, 255, 255), image_R); // Filter the image for pink

    image_B = imread("BlueShortCut.png");
    cvtColor(image_B, image_B, COLOR_BGR2HSV); // Convert the image to HSV
    inRange(image_B, Scalar(140, 125, 255), Scalar(200, 255, 255), image_B); // Filter the image for pink

    image_Ball = imread("Football.png");
    cvtColor(image_Ball, image_Ball, COLOR_BGR2HSV); // Convert the image to HSV
    inRange(image_Ball, Scalar(140, 125, 255), Scalar(200, 255, 255), image_Ball);

    image_Ramp = imread("InclineMeasurement.png");
    cvtColor(image_Ramp, image_Ramp, COLOR_BGR2HSV); // Convert the image to HSV
    inRange(image_Ramp, Scalar(140, 125, 255), Scalar(200, 255, 255), image_Ramp);

    image_Light = imread("StopLight.png");
    cvtColor(image_Light, image_Light, COLOR_BGR2HSV); // Convert the image to HSV
    inRange(image_Light, Scalar(140, 125, 255), Scalar(200, 255, 255), image_Light);

    image_Count = imread("ShapeCounter.png");
    cvtColor(image_Count, image_Count, COLOR_BGR2HSV); // Convert the image to HSV
    inRange(image_Count, Scalar(140, 125, 255), Scalar(200, 255, 255), image_Count);

    image_Dist = imread("DistanceMeasurement.png");
    cvtColor(image_Dist, image_Dist, COLOR_BGR2HSV); // Convert the image to HSV
    inRange(image_Dist, Scalar(140, 125, 255), Scalar(200, 255, 255), image_Dist);

    imshow("before", image_BW);
    waitKey();

    vector<vector<Point> > contours; // Variable for list of contours
    vector<Vec4i> hierarchy; // Variable for image topology data
    findContours(image_BW, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, Point(0, 0)); // Calculate the contours and store them
    int n = contours.size();

    cout << "Number of contours:" << n << endl;

    int a = 0;
    int b = 0;
    int i = 0;

    for (i = 0; i<n; i++)
    {
       int area = contourArea(contours[i]);

       if (area>a)
       {
           a = area;
           b = i;
       }
    }

    cout << "biggest contour:" << b << endl;

    int m = 0;

    Mat drawing = Mat::zeros( image_BW.size(), CV_8UC3);
    for( size_t m = 0; m<contours.size(); m++)
    {
        cout << "Cn: " << (int)m << endl;
        Scalar color = Scalar(255, 255, 255);
        drawContours(drawing, contours, (int)m, color, 2, 8, hierarchy, 0, Point() );
    }
    imshow("contours", drawing);
    waitKey();
    //cout << contours[b] << endl;

    std::vector< std::vector<cv::Point> > approxedcontours(contours.size());
    for(int j = 0; j < contours.size(); j++)
    {
        cv::approxPolyDP(contours[j],approxedcontours[j], 10, true);
    }
    cout << approxedcontours[b] << endl;

    image_BW = transformPerspective(approxedcontours[b], image_BW, 320, 240);
    if(image_BW.cols > 0 && image_BW.rows > 0)
        imshow("transform", image_BW);
    waitKey();



    float match_G = compareImages(image_BW, image_G);
    float match_Y = compareImages(image_BW, image_Y);
    float match_R = compareImages(image_BW, image_R);
    float match_B = compareImages(image_BW, image_B);
    float match_Ball = compareImages(image_BW, image_Ball);
    float match_Ramp = compareImages(image_BW, image_Ramp);
    float match_Light = compareImages(image_BW, image_Light);
    float match_Count = compareImages(image_BW, image_Count);
    float match_Dist = compareImages(image_BW, image_Dist);

    cout << "percentage green:" << match_G << endl;
    cout << "percentage yellow:" << match_Y << endl;
    cout << "percentage red:" << match_R << endl;
    cout << "percentage blue:" << match_B << endl;
    cout << "percentage ball:" << match_Ball << endl;
    cout << "percentage light:" << match_Light << endl;
    cout << "percentage ramp:" << match_Ramp << endl;
    cout << "percentage count:" << match_Count << endl;
    cout << "percentage distance:" << match_Dist << endl;

    char colour = 'Z';

    if ((match_G>match_Y) && (match_G>match_R) && (match_G>match_B) && (match_G>match_Dist) && (match_G>match_Ball) && (match_G>match_Ramp) && (match_G>match_Light) && (match_G>match_Count))
    {
        colour = 'A';
    }

    if ((match_Y>match_G) && (match_Y>match_R) && (match_Y>match_B) && (match_Y>match_Dist) && (match_Y>match_Ball) && (match_Y>match_Ramp) && (match_Y>match_Light) && (match_Y>match_Count))
    {
        colour = 'B';
    }

    if ((match_R>match_Y) && (match_R>match_G) && (match_R>match_B) && (match_R>match_Dist) && (match_R>match_Ball) && (match_R>match_Ramp) && (match_R>match_Light) && (match_R>match_Count))
    {
        colour = 'C';
    }

    if ((match_B>match_Y) && (match_B>match_R) && (match_B>match_G) && (match_B>match_Dist) && (match_B>match_Ball) && (match_B>match_Ramp) && (match_B>match_Light) && (match_B>match_Count))
    {
        colour = 'D';
    }

    if ((match_Ball>match_Y) && (match_Ball>match_R) && (match_Ball>match_G) && (match_Ball>match_B) && (match_Ball>match_Light) && (match_Ball>match_Dist) && (match_Ball>match_Ramp) && (match_Ball>match_Count))
    {
        colour = 'E';
    }

    if ((match_Light>match_Y) && (match_Light>match_R) && (match_Light>match_G) && (match_Light>match_B) && (match_Light>match_Dist) && (match_Light>match_Ball) && (match_Light>match_Ramp) && (match_Light>match_Count))
    {
        colour = 'F';
    }

    if ((match_Ramp>match_Y) && (match_Ramp>match_R) && (match_Ramp>match_G) && (match_Ramp>match_B) && (match_Ramp>match_Light) && (match_Ramp>match_Ball) && (match_Ramp>match_Dist) && (match_Ramp>match_Count))
    {
        colour = 'G';
    }

    if ((match_Count>match_Y) && (match_Count>match_R) && (match_Count>match_G) && (match_Count>match_B) && (match_Count>match_Light) && (match_Count>match_Ball) && (match_Count>match_Ramp) && (match_Count>match_Dist))
    {
        colour = 'H';
    }

    if ((match_Dist>match_Y) && (match_Dist>match_R) && (match_Dist>match_G) && (match_Dist>match_B) && (match_Dist>match_Light) && (match_Dist>match_Ball) && (match_Dist>match_Ramp) && (match_Dist>match_Count))
    {
        colour = 'I';
    }

    switch(colour) {
      case 'A' :
         cout << "Green" << endl;
         break;
      case 'B' :
         cout << "Yellow" << endl;
         break;
      case 'C' :
         cout << "Red" << endl;
         break;
      case 'D' :
         cout << "Blue" << endl;
         break;
      case 'E' :
         cout << "Ball" << endl;
         break;
      case 'F' :
         cout << "Light" << endl;
         break;
      case 'G' :
         cout << "Ramp" << endl;
         break;
      case 'H' :
         cout << "Count" << endl;
         break;
      case 'I' :
         cout << "Distance" << endl;
         break;
      default :
         cout << "ERROR" << endl;
    }


}
